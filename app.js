var winningscore, prev_dice, scores, roundScore, activePlayer, gamePlaying;



function init() {
    scores = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    gamePlaying = true;
    winningscore = 100;
        
    //change CSS 
    document.getElementById('dice-1').style.display = 'none';
    document.getElementById('dice-2').style.display = 'none';
    
    //using getElementById - no need to use # symbol
    document.getElementById('score-0').textContent = 0;
    document.getElementById('score-1').textContent = 0;
    document.getElementById('current-0').textContent = 0;
    document.getElementById('current-1').textContent = 0;
    
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    
    document.querySelector('.player-0-panel').classList.add('active');
    
    document.querySelector('.final-score').disabled = false;
    document.getElementsByName('final-score')[0].value = "";
    document.getElementsByName('final-score')[0].placeholder = "Final Score";
}

init();


//anonymous function- function used directly without having a name so it can't be reused, example in next code
                      
document.querySelector('.btn-roll').addEventListener('click', function () {
    if (gamePlaying) {
        
        var inputt = document.querySelector('.final-score').value;
        
        if (inputt) {
            winningscore = inputt;
            document.querySelector('.final-score').disabled = true;
        } else {
            document.querySelector('.final-score').value = 100;
            document.querySelector('.final-score').disabled = true;
        }
        
        // 1. Random number
        var dice1 = Math.floor(Math.random() * 6) + 1;
        var dice2 = Math.floor(Math.random() * 6) + 1;

        // 2. Display the result
        document.getElementById('dice-1').style.display = 'block';
        document.getElementById('dice-2').style.display = 'block';
        document.getElementById('dice-1').src = 'dice-' + dice1 + '.png';
        document.getElementById('dice-2').src = 'dice-' + dice2 + '.png';
        
        // 3. Update the round score
        
        if (dice1 !== 1 && dice2 !==1) {
            //Add score
            roundScore += dice1 + dice2;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        } else {
            nextPlayer();
        }
        
        
        
    }
});

document.querySelector('.btn-hold').addEventListener('click', function () {
    if (gamePlaying) {
        
        var inputt = document.querySelector('.final-score').value;
        
        //undefined, 0, null or "" are Coerced to false
        //anything else to true

        if (inputt) {
            winningscore = inputt;
            document.querySelector('.final-score').disabled = true;
        }
        
        //1. Add Current score to GLOBAL Score
        scores[activePlayer] += roundScore;

        //2. Update the UI
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

        if (scores[activePlayer] >= winningscore) {
            //3. Check if player won the game
            document.querySelector('#name-' + activePlayer).textContent = 'Winner';
            document.getElementById('dice-1').style.display = 'none';
            document.getElementById('dice-2').style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            gamePlaying = false;
        } else {
            //4. Next Player
            nextPlayer();
        }
    }
});
                      
document.querySelector('.btn-new').addEventListener('click', init);

function nextPlayer() {
    
    //Next player
    //activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    activePlayer ^= 1;
    roundScore = 0;

    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';

    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');

    prev_dice = 0;
    

    setTimeout(function () {
        document.getElementById('dice-1').style.display = 'none';
        document.getElementById('dice-2').style.display = 'none';
    }, 500);
}